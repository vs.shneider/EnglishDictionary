import re

pattern1 = '\d{2}/\d{2}/\d{4}(.)*'
pattern2 = 'https.*'
pattern3 = '\d+'
pattern4 = '[a-z,A-Z]'
pattern5 = '[а-я,А-Я]'
pattern6 = '\s.*'
replace = ''


def get_raw_lines():
    with open('resources/words.txt', 'r', encoding='utf-8') as f:
        return [line.rstrip('\n') for line in f]


def write_file(lines):
    with open('resources/words.csv', 'w', encoding='utf-8') as f:
        f.writelines(lines)


raw_lines = get_raw_lines()

lines = [
    'Выучено' + ';' + 'Теги' + ';' + 'Слово' + ';' + 'Транскрипция' + ';' + 'Перевод' + ';' + 'Дополнительный перевод' + ';' + 'Примеры' + ';' + '\n']
for line in raw_lines:
    sub1 = str.strip(re.sub(pattern1, replace, line))
    sub2 = str.strip(re.sub(pattern2, replace, sub1))
    sub3 = re.sub(pattern3, replace, sub2)
    if not sub3 == '':
        translation = str.strip(re.sub(pattern4, replace, sub3))
        sub5 = str.strip(re.sub(pattern5, replace, sub3))
        word = str.strip(re.sub(pattern6, replace, sub5))

        lines.append('0%' + ';' + 'popular' + ';' + word + ';' + ' ' + ';' + translation + ';' + ';' + ';' + '\n')

write_file(lines)
